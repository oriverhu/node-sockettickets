var socket = io();
//Escuchar información
socket.on('connect', function() {
    console.log('Conectado al Servidor');
})
socket.on('disconnect', function() {
    console.log('Conexión perdida');
})
socket.on('estadoActual', function(data) {
    $("#lblNuevoTicket").html(data.actual);
})

$('button').on('click', function() {
    socket.emit('siguienteTicket', (resp) => {
        $("#lblNuevoTicket").html(resp.resp);
    })
})