var socket = io();

var searchParams = new URLSearchParams(window.location.search); // objeto que obtiene parametros de la url

if (!searchParams.has('escritorio')) {
    window.location = 'index.html';
    throw new Error('El escritorio es necesario');
}

var escritorio = searchParams.get('escritorio');

$('h1').append(' ' + escritorio);

$('button').on('click', function() {
    socket.emit('atenderTicket', { escritorio }, (resp) => {
        if (!resp.numero) {
            alert(resp)
        } else {
            $('small').text(`Ticket ${resp.numero}`);
        }
    })
})