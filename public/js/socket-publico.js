var socket = io();

socket.on('estadoActual', function(data) {
    if (data.ultimos4.length > 0) {
        handleChange(data.ultimos4);
    }
})

socket.on('ultimos4', (data) => {
    if (data.ultimos4.length > 0) {

        var audio = new Audio('./audio/new-ticket.mp3');
        audio.play();

        handleChange(data.ultimos4);
    }
})

function handleChange(ultimos4) {
    $("#lblTicket1").html('Ticket ' + ultimos4[0].numero);
    $("#lblEscritorio1").html('Escritorio ' + ultimos4[0].escritorio);

    var array = ultimos4.splice(1);

    array.forEach((e, i) => {
        $("#lblTicket" + (i + 2)).html('Ticket ' + e.numero);
        $("#lblEscritorio" + (i + 2)).html('Escritorio ' + e.escritorio);
    });
}