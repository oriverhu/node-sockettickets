const { io } = require('../server');
const { TicketControl } = require('./../classes/ticket-control');

const ticketCtr = new TicketControl();

io.on('connection', (client) => {

    client.on('siguienteTicket', (callback) => {
        if (callback) {
            let nuevoTicket = ticketCtr.siguiente();
            callback({
                resp: nuevoTicket
            });
        }
    });

    client.emit('estadoActual', {
        actual: ticketCtr.getUltimoTicket(),
        ultimos4: ticketCtr.getUltimos4()
    });

    client.on('atenderTicket', (data, callback) => {
        if (!data.escritorio) {
            return callback({
                err: true,
                mensaje: 'El escritorio es necesario'
            })
        }

        let atender = ticketCtr.atenderTicket(data.escritorio);
        callback(atender);


        // actualizar cambios en los ultimos 4

        client.broadcast.emit('ultimos4', {
            ultimos4: ticketCtr.getUltimos4()
        });

    })
});